window.addEventListener("DOMContentLoaded", function () {
    // Ñîçäàåì êîíñòàíòû
    const PRICE_1 = 750;
    const PRICE_2 = 1350;
    const PRICE_3 = 1650;
    const EXTRA_PRICE = 0;
    const RADIO_1 = 600;
    const RADIO_2 = 300;
    const RADIO_3 = 900;
    const CHECKBOX = 750;
    // Ñîçäàåì ïåðåìåííûå
    let price = PRICE_1;
    let extraPrice = EXTRA_PRICE;
    let result;
    // Ñîçäàåì html ýëåìåíòû
    let calc = document.getElementById("calc");
    let myNum = document.getElementById("number");
    let resultSpan = document.getElementById("result-span");
    let select = document.getElementById("select");
    let radiosDiv = document.getElementById("radio-div");
    let radios = document.querySelectorAll("#radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("check-div");
    let checkbox = document.getElementById("checkbox");
    // Ôóíêöèÿ îáðàáîòêè èçìåíåíèé â select
    select.addEventListener("change", function (event) {
        // Âûáðàííûé ýëåìåíò option
        let option = event.target;
        // Ñìîòðèì êàêîé âûáðàí è îòîáðàæàåì ýëåìåíòû
        if (option.value === "1") {
            // Âêëþ÷àåì ðàäèîêíîïêè è âûêëþ÷àåì ÷åêáîêñ
            radiosDiv.classList.add("d-none");
            checkboxDiv.classList.add("d-none");
            // Èçìåíÿåì äîï. öåíó
            extraPrice = EXTRA_PRICE;
            // Èçìåíÿåì öåíó çà øòóêó
            price = PRICE_1;
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = RADIO_1;
            price = PRICE_2;
            // Ñáðàñûâàåì ðàäèîêíîïêó
            document.getElementById("radio1").checked = true;
        }
        if (option.value === "3") {
            checkboxDiv.classList.remove("d-none");
            radiosDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = PRICE_3;
            // Ñáðàñûâàåì ÷åêáîêñ
            checkbox.checked = false;
        }
    });
    // Îáðàáàòûâàåì íàæàòèÿ íà ðàäèîêíîïêè
    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {
            // Âûáðàííàÿ ðàäèîêíîïêà
            let radio = event.target;
            // Óçíàåì êîíêðåòíóþ ðàäèîêíîïêó
            if (radio.value === "r1") {
                // Èçìåíÿåì äîï. öåíó
                extraPrice = RADIO_1;
            }
            if (radio.value === "r2") {
                extraPrice = RADIO_2;
            }
            if (radio.value === "r3") {
                extraPrice = RADIO_3;
            }
        });
    });
    // Îáðàáàòûâàåì íàæàòèå ÷åêáîêñà
    checkbox.addEventListener("change", function () {
        // Åñëè íàæàò, ìåíÿåì äîï. öåíó, èíà÷å ñáðàñûâàåì
        if (checkbox.checked) {
            extraPrice = CHECKBOX;
        } else {
            extraPrice = EXTRA_PRICE;
        }
    });
    // Îáðàáàòûâàåì ëþáûå èçìåíåíèÿ â êàëüêóëÿòîðå
    // Åñëè îíè åñòü, ïåðåñ÷èòûâàåì èòîãîâóþ öåíó
    calc.addEventListener("change", function () {
        // Çàùèòà îò îòðèöàòåëüíîãî êîëè÷åñòâà
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        // Ïåðåñ÷åò ðåçóëüòàòà
        result = (price + extraPrice) * myNum.value;
        // Âñòàâêà ðåçóëüòàòà â HTML
        resultSpan.innerHTML = result;
    });
});